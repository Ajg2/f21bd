import sys

import scripts.MongoDB as smdb
import scripts.neo4j as sn4j
import scripts.SQL as ssql
import scripts.execute as execute


def menuOption():
    print("Select an option:")
    print("1 : Export JSON from SQL")
    print("2 : Import JSON into MongoDB")
    print("3 : Execute Queries on MongoDB or Neo4j")
    print("4 : Generate MongoDB Schema")
    print("0 : Kill system")

    return int(input())


def runTask(inputchoice):
    if (inputchoice == 1):
        """
        MySQL Task
        """
        ssql.exportAsJSON()

    elif(inputchoice == 2):
        smdb.startLoad()

    elif(inputchoice == 3):
        jsonpath= input("Please enter JSON file path...\n")
        choice = int(input("Input desired execution system 0: MongoDB , 1: Neo4j \n"))
        execute.execute(jsonpath,choice)

    elif(inputchoice == 4):
        smdb.genSchema()
    
    elif(inputchoice == 0):
        sys.exit(0)



def runApp():
    while True:
        # Get back input option
        x = menuOption()

        runTask(x)



if __name__ == "__main__":
    runApp()

import json
from pprint import pprint

def execute(json_file,choice):
    with open(json_file) as f:
        data = json.load(f)
    if(choice == 0):
        mongodb(data,choice)
    else:
        neo4j(data,choice)

def mongodb(data,choice):
    print("Executing Mongodb Queries...")
    num_queries = len(data[choice]['queries']['query'])
    i=0
    for i in range(num_queries):
        query = data[choice]['queries']['query'][i]['code']
        print(query)

def neo4j(data,choice):
    print("Executing Neo4j Queries...")
    num_queries = len(data[choice]['queries']['query'])
    i=0
    for i in range(num_queries):
        query = data[choice]['queries']['query'][i]['code']
        print(query)

execute('query_structure.json',0)

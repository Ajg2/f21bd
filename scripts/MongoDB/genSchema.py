import os
import errno
import simplejson as json
from pymongo_schema.extract import extract_pymongo_client_schema
from pymongo_schema.export import transform_data_to_file

from config import MONGO_DB
from .constants import db, tunnel


def genSchema():
    try:
        # Start tunnel
        tunnel.start()
        # Choose db
        #mongodb = db[MONGO_DB]

        
        schema = extract_pymongo_client_schema(db, database_names=MONGO_DB)

        filename = "data/"+MONGO_DB+"_SCHEMA/"

        transform_data_to_file(schema, ['json', 'md'], output=filename, without_counts=True)

        schema=schema[MONGO_DB]

        for key in schema.keys():
            temp = filename + key + ".json"
            
            if not os.path.exists(os.path.dirname(temp)):
                try:
                    os.makedirs(os.path.dirname(temp))
                except OSError as exc: # Guard against race condition
                    if exc.errno != errno.EEXIST:
                        raise

            with open(temp, "w") as myfile:
                myfile.write(json.dumps(schema[key], indent=2, ensure_ascii=False, encoding="utf-8"))


    except Exception as e:
        print("Exception occured, %s" % type(e))

    finally:
        db.close()
        tunnel.stop()
from sshtunnel import SSHTunnelForwarder,create_logger
from config import SSH_HOST, SSH_PORT, MONGO_SERVER, MONGO_PORT, MONGO_USERNAME, MONGO_PASSWORD, MONGO_DB, SSH_USER, SSH_PASS
import pymongo
import pprint
import socket


def getTunnel():
    tunnel = SSHTunnelForwarder(
    (SSH_HOST, SSH_PORT),
    ssh_username=SSH_USER,
    ssh_password=SSH_PASS,
    local_bind_address=('127.0.0.1', MONGO_PORT),
    remote_bind_address=(MONGO_SERVER, MONGO_PORT)
    )

    return tunnel


def getDB():
        connection = pymongo.MongoClient('mongodb://' + MONGO_USERNAME + ':' + MONGO_PASSWORD + '@127.0.0.1/' + MONGO_DB) # server.local_bind_port is assigned local port
        return connection

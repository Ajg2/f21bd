import simplejson as json

from config import MONGO_DB
from .constants import db, tunnel


def startLoad():
    print("Please enter JSON filename in 'data/'")
    x = input()

    loadJSONtoMongo(x)


def loadJSONtoMongo(file):
    try:
        # Start tunnel
        tunnel.start()
        # Choose db
        mongodb = db[MONGO_DB]

        # Read file inputted
        print("Opening data/%s" % file)
        with open('data/' + file) as f:
            file_data = json.load(f)

        print("Successfully opened")

        # Get keys in dict
        keys = file_data.keys()
        for key in keys:
            print("Inserting %d %s queries from file..." % (len(file_data[key]), key))

            # Delete everything in collection first
            mongodb[key].delete_many({})

            # Insert each item into key collection
            for item in file_data[key]:
                mongodb[key].insert(item)

            print("Done")

    except FileNotFoundError as e:
        print("Couldn't find file in data/%s \nCheck spelling and try again.." % file)

    except Exception as e:
        print("Exception occured, %s" % type(e))

    finally:
        db.close()
        tunnel.stop()
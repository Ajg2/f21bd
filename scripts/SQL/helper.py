
import json
import mysql.connector
from sshtunnel import SSHTunnelForwarder, create_logger

from config import SSH_HOST, SSH_PORT, SSH_USER, SSH_PASS, SQL_SERVER, SQL_PORT, SQL_USERNAME, SQL_PASSWORD, SQL_DATABASE


def getData(json_file):
    with open(json_file) as f:
        data = json.load(f)
    
    return data


def getTunnel():
    tunnel = SSHTunnelForwarder(
    (SSH_HOST, SSH_PORT),
    ssh_username=SSH_USER,
    ssh_password=SSH_PASS,
    local_bind_address=('127.0.0.1', SQL_PORT),
    remote_bind_address=(SQL_SERVER, SQL_PORT),
    logger=create_logger(loglevel=0)
    )

    return tunnel


def getDB():
    """
    Returns Database object with connection details
    """
    return mysql.connector.connect(
    host="127.0.0.1",
    user=SQL_USERNAME,
    passwd=SQL_PASSWORD,
    database=SQL_DATABASE
    )


def merge_dicts(dict1, dict2):
    for key in dict2.keys():
        # Get value in dict1
        t1 = dict1.get(key)

        # put in dict1 as no key
        if t1 is None:
            dict1[key] = dict2.get(key)

        # key does exist
        else:
            # Get value in dict 2
            t2 = dict2.get(key)

            # If both are dicts, recursive merge
            if type(t1) == dict and type(t2) == dict:
                dict1[key] = merge_dicts(t1, t2)

            # Both aren't dicts so put both in lists and merge
            else:
                t3 = [t1] + [t2]
                dict1[key] = t3

    return dict1


def dot_recursive(key, value):
    # if value list on last element then rturn
    if len(value) == 1:
        return {key: value[0]}

    # Put value as recursive call, splitting the list
    # from position 0
    return {key: dot_recursive(value[0], value[1:])}


def dot_to_json(items):
    # Dict to return
    output = {}

    # Iterate through items
    for item in items:
        # item[0] is keys in dot notation
        # e.g `track.composer`
        keys = item[0]
        value = item[1]

        # Split the keys dots
        keys = keys.split(".")
        # print("keys: %s , value: %s" % (keys, value))

        if len(keys) > 1:
            # add value to end of keys
            keys.append(value)

            # Create nested dict from keys with value as last element
            new_dict = dot_recursive(keys[0], keys[1:])

            # Merge the nested dict with the main dict
            output = merge_dicts(output, new_dict)
           
        else:
            # Set first key to value
            output[keys[0]] = value

    return output
import simplejson as json
import copy

from .constants import tunnel
from .helper import getDB

employeeKeys = ["EmployeeId", "ReportsTo", "FirstName", "LastName", "Title", "Phone", "Email", "Fax"]

def queryMutator(queryName, res):
    if queryName == "Playlist":
        # Parses 'Tracks' string into array
        tracks_data = json.loads(res[2][1])
        # loop over 'Tracks' array
        for i in range(len(tracks_data)):
            # Convert strings to an int value
            tracks_data[i]['Track']['Milliseconds'] = int(tracks_data[i]['Track']['Milliseconds'])
            tracks_data[i]['Track']['Bytes'] = int(tracks_data[i]['Track']['Bytes'])
            # tracks_data[i]['Track']['UnitPrice'] = int(tracks_data[i]['Track']['UnitPrice'])
        # Update Track element
        res[2] = (res[2][0], tracks_data)

    return res


def allQueryMutator(collection, all_res):
    if collection == "Employee":
        # Create dict of all employees
        all_res_dict = dict()
        for res in all_res:
            all_res_dict[res[0][1]] = dict(res)

        for i in range(len(all_res)):
            all_res[i] = all_res[i] +  [ ('Manages', employeeManages(all_res_dict, dict(all_res[i]))) ]

            # Create a dict temp
            dict_temp = employeeReportsTo(all_res_dict, dict(all_res[i]))
            all_res[i] = [(k,v) for k,v in dict_temp.items()]  

    return all_res


def employeeReportsTo(all_res_dict, cur_dict):
    employeeId = cur_dict.get('ReportsTo')

    if employeeId != None:
        # Create deep copy from dict
        next_dict = copy.deepcopy(all_res_dict[employeeId])
        # Match values to keys and create dict
        next_dict = {k: v for k, v in next_dict.items() if k in employeeKeys}
        # Update cur_dict to nested dict from employee's id
        cur_dict['ReportsTo'] = employeeReportsTo(all_res_dict, next_dict)

    return cur_dict


def employeeManages(all_res_dict, cur_dict):
    employeeId = cur_dict.get('EmployeeId')
    output = []

    for key in all_res_dict.keys():
        if key == employeeId:
            continue

        if all_res_dict.get(key)['ReportsTo'] == employeeId:
            output.append({k:v for k,v in all_res_dict.get(key).items() if k in employeeKeys}) 

    if len(output) > 0:
        return output
    return None
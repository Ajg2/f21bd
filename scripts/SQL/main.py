import sys
import time

from bson import json_util
import simplejson as json
from pprint import pprint
from .constants import tunnel
from .queryHandler import queryMutator, allQueryMutator
from .helper import getDB, getData, dot_to_json




def getAllTables():
    """
    Output all tables in MySQL database
    """
    mydb = getDB()

    mycursor = mydb.cursor()
    mycursor.execute("SHOW TABLES")

    return mycursor.fetchall()


def exportAsJSON():
    """
    Generate each table as JSON
    """
    try:
        tunnel.start()

        # Blocks until local is bounded
        try:
            while not tunnel.local_is_up(tunnel.local_bind_address):
                pass
        except Exception as e:
            print(e)

        # Loads JSON from file
        json_q = getData("scripts/SQL/table_queries.json")

        # Gets all queries
        json_q = json_q['queries']


        # Gets all tables in db
        #allTables = getAllTables()

        # Gets db object
        mydb = getDB()

        with open("data/mongodata.json", "w") as myfile:
            myfile.write("{")

            index = 0
            # Iterate through all talbles
            for collection in json_q:
                #index = json_q.index(collection)
                #print("Index: %s, Len: %s" % (index, len(json_q)))

                # Gets cursor for db
                mycursor = mydb.cursor()

                print("Working on collection %s" % collection)
                myfile.write('\n "' + collection + '": [\n')

                for querys in json_q[collection]:
                    # Get queries for table
                    desc = querys['description']
                    query = querys['query']

                    # Execute query
                    mycursor.execute(query)
                    
                    if mycursor.column_names != ():
                        # Get column names from that query
                        column_names = mycursor.column_names

                        # Get results
                        result = mycursor.fetchall()

                        # Map results to column names
                        map_res = [list(zip(column_names, res)) for res in result]

                        # Mutate all results at once
                        map_res = allQueryMutator(collection, map_res)

                        counter = 0
                        # Convert each row's dot notation to dict
                        for res in map_res:
                            counter += 1
                            # Update res with query specific mutates
                            res = queryMutator(collection, res)
                            # Write row to file
                            myfile.write(json.dumps(dot_to_json(res), indent=2, ensure_ascii=False, encoding='utf-8'))
                            if counter < len(map_res):
                                myfile.write(",")
                            myfile.write("\n")
                                
                myfile.write("]")

                if index < len(json_q)-1:
                    myfile.write(",")
                myfile.write("\n")

                index += 1

            myfile.write("}\n")

    except Exception as e:
        print("Exception")
        print(e)

    finally:
        mydb.close()
        tunnel.stop()

import simplejson as json
from pprint import pprint
from scripts.MongoDB.constants import tunnel
from scripts.MongoDB.helper import getDB
import pymongo as pymongo

def execute(json_file,choice):
        with open(json_file) as f:
                data = json.load(f)
        if(choice == 0):
                mongodb(data,choice)
        else:
                neo4j(data,choice)

def mongodb(data,choice):
        tunnel.start()
        mydb = getDB()
        db = mydb["ajg2"]
        print("Executing Mongodb Queries...")
        num_queries = len(data[choice]['queries']['query'])
        i=0
        for i in range(num_queries):
                query = data[choice]['queries']['query'][i]['code']
                collection = db[data[choice]['queries']['query'][i]['collection']]
                print('\n')
                print("************************************************************ \n")
                print('Query ' + str(i) + ' execution... \n')
                description = data[choice]['queries']['query'][i]['description']
                print('Description: ' + str(description) + '\n')
                numentries = 0
                x = collection.aggregate(query)
                x = list(x)
                for result in x:
                        pprint(result)
                        numentries += 1
                print('\n')
                msg = ('Number of Results: ' + str(numentries))
                row = len(msg)
                h = ''.join(['+'] + ['-' *row] + ['+'])
                result= h + '\n'"|"+msg+"|"'\n' + h
                print(result)
                print('\n')
                print("End of Query " + str(i) + " execution... \n")
                print("************************************************************ \n")
                
        mydb.close()
        tunnel.stop()

def pickMongoOperation(query):
        
        query.split("")

def neo4j(data,choice):
        print("Executing Neo4j Queries...")
        num_queries = len(data[choice]['queries']['query'])
        i=0
        for i in range(num_queries):
                query = data[choice]['queries']['query'][i]['code']
                print(query)

